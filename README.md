# fix-perms

## get_perms

Run: 

```sh
python get_perms PATH1 [PATH2]
```

- `PATH1`: The relative or absolute path were to get the filesystem User:Group ids for entries
- `PATH2`: The absolute path prefix to remove

Result into a `fsperms.csv` file where values separated by `;` are :
- The original absolute path (file, directory or symlink)
- The UID
- The GID


## Use Case : Recover full disk perms from a source instance

## If the instace is down - Start in recovery mode

Consider the source instance to be started in recovery mode in order to mount the FS read only.
Enter the source instance recovery mode and setup the environnement to run the Python script.
We can't install via pipenv : the rescue FS is limited to 1Go and all tools needed exceed the disk capacity

```bash
# Upgrade the system
apt update && apt upgrade -y
# Install python 3.10 repository
apt install software-properties-common -y
add-apt-repository ppa:deadsnakes/ppa
apt install python3.10
# Install GIT and clone the repo
apt install git
git clone https://gitlab.com/ftoral/fix_perms.git
# Then run
cd fix_perms/
python3.10 get_perms /var ''
```