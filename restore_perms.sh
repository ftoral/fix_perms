#!/bin/bash

# To do a dry run, set a DRY_RUN variable, or uncomment the following line
DRY_RUN=yes

function usage() {
    cat << EOF

Usage : $0 PREFIX_PATH [FSPERMS_FILE]

    PREFIX_PATH  : The path from were to apply the chown
    FSPERMS_FILE : Result file provided by get_perms script ('fsperms.csv' by default)

EOF
}

# Test arguments number
if (( $# < 1 )); then
    usage
    exit 1
fi

# Test if path exists
export prefix="$1"
if [ ! -d "${prefix}" ]; then
    echo "Not a path : ${prefix}"
    usage
    exit 1
fi

# Test if fsperms file exists
export FSPERMS_FILE="${2:-fsperms.csv}"
if [ ! -r "${FSPERMS_FILE}" ]; then
    echo "Not accessible : ${FSPERMS_FILE}"
    usage
    exit 1
fi

export chown_cmd="chown -R"
export chmod_cmd="sudo chmod"
if [ -n "${DRY_RUN}" ]
then
    export chown_cmd="echo ${chown_cmd}"
#    export chmod_cmd="echo ${chmod_cmd}"
fi

cat ${FSPERMS_FILE} \
    | awk -F';' '{ print $1 " " $2 " " $3 " " $4 " " $5 }' \
    | xargs -L 1 sh -c 'echo "entry: ($prefix)$1 $5 [$2:$3] ";$chown_cmd $2:$3 $prefix$1' sh

echo 
echo " --- Run the following lines to set the Setuid, Setgid and Stiky bits (could not be set by script)"
cat ${FSPERMS_FILE} \
    | awk -F';' '{ print $1 " " $5 }' \
    | xargs -L 1 sh -c '(( ${2:0:1} != 0 )) && echo "$chmod_cmd $2 $prefix$1"' sh
