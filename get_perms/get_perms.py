import glob
import io
import os
from pathlib import Path

debug=False
result_filename="fsperms.csv"

#=====================

def walkForUserAndGroup(root_dir, prefix='', file=None):
    # walkForUserAndGroup_walk(root_dir, prefix)
    walkForUserAndGroup_scandir(root_dir, prefix, file)

#=====================

def getUidAndGidAndMode(file):
    if isinstance(file, os.DirEntry):
        lstat = file.stat(follow_symlinks=False)
    else:
        lstat = os.lstat(file)
    #print(f"getUidAndGidAndMode: file={str(file)} | UG= {lstat.st_uid}:{lstat.st_gid}") 
    return (lstat.st_uid, lstat.st_gid, lstat.st_mode)

#--------------
def testForSameUG(pathFile: os.DirEntry, ugid_dir=(-1, -1), ugid=(-1, -1)):
    if (ugid[0] == -1 and ugid[1] == -1):
        ugid = getUidAndGidAndMode(pathFile)
    test = (ugid[0] == ugid_dir[0] and ugid[1] == ugid_dir[1])
    #print(f"TEST {test} FILE= {str(pathFile)} ")
    return test

#--------------
def testForSpecialMode(pathFile: os.DirEntry, mode=0):
    if (mode == 0):
        ugidmode = getUidAndGidAndMode(pathFile)
        mode = ugidmode[2]
    if (int(oct(mode)[-4:-3]) != 0):
        # print(f"mode {mode} | {oct(mode)[-4:-3]} | file: {pathFile}")
        return True
    return False

#--------------
def tuplePathUgid(pathFile: os.DirEntry, prefix: str, ugidmode=(-1, -1, -1)):

    # Get the Uid and Gid if not provided
    if (ugidmode[0] == -1 and ugidmode[1] == -1):
        ugidmode = getUidAndGidAndMode(pathFile)

    # Get the target path by removing the prefix on our current FS
    chunkToPrefixPath = os.path.abspath(pathFile).removeprefix(prefix)
    if chunkToPrefixPath == '': chunkToPrefixPath = '/' # Case of the FS root

    # Get the File mode in octal
    oct_mode=oct(ugidmode[2])[-4:]

    return (chunkToPrefixPath, ugidmode[0], ugidmode[1], ugidmode[2], oct_mode)

#--------------
def printFileInfoTuple(fileinfos=('', -1, -1), file=None, is_symlink=False):
    flag = ""
    if debug:
        if is_symlink: flag = "(SYM)"
        print(f"ENTRY{flag} = {fileinfos[0]} | UG= {fileinfos[1]}:{fileinfos[2]} | MODE = {fileinfos[3]}/{fileinfos[4]}")
    if file is not None and isinstance(file, io.TextIOWrapper):
        file.write(f"{fileinfos[0]};{fileinfos[1]};{fileinfos[2]};{fileinfos[3]};{fileinfos[4]}{os.linesep}")

#--------------
def walkForUserAndGroup_scandir(root_dir, prefix='', file=None):
    ugid_root = getUidAndGidAndMode(root_dir)
    dir_entries = os.scandir(root_dir)
    for dir_entry in dir_entries:
        ugidmode = getUidAndGidAndMode(dir_entry)
        if dir_entry.is_dir() and not dir_entry.is_symlink():
            if not testForSameUG(dir_entry, ugid_root, ugidmode) or testForSpecialMode(dir_entry):
                fileinfos = tuplePathUgid(dir_entry, prefix, ugidmode)
                printFileInfoTuple(fileinfos, file)
            # recurse
            walkForUserAndGroup_scandir(dir_entry.path, prefix, file)
        else:
            if not testForSameUG(dir_entry, ugid_root, ugidmode) or testForSpecialMode(dir_entry):
                fileinfos = tuplePathUgid(dir_entry, prefix, ugidmode)
                printFileInfoTuple(fileinfos, file, dir_entry.is_symlink())
