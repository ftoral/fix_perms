import sys
import os

import get_perms

get_perms.debug=True

if __name__ == "__main__":
    path = str(sys.argv[1])
    abspath = os.path.abspath(path)
    prefix = str(sys.argv[2])
    abspath_prefix = str(os.path.abspath(prefix))
    with open(get_perms.result_filename, "a", encoding="utf-8") as result_file:
        result_file.truncate(0)

        # Start with root
        fileinfos = get_perms.tuplePathUgid(abspath, abspath_prefix)
        get_perms.printFileInfoTuple(fileinfos, result_file)

        # Walk over the fs tree
        get_perms.walkForUserAndGroup(abspath, abspath_prefix, file=result_file)
